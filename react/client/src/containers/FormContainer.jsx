import React, { Component } from "react";

/* Import Components */
import Input from "../components/Input";
import Button from "../components/Button";

class FormContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      newUser: {
        username: "",
        email: "",
        password : "",
        exprience : "",
        level : "",
      }
    };
   
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    this.handleInput = this.handleInput.bind(this);
  }


 

  

  handleAge(e) {
    let value = e.target.value;
    this.setState(
      prevState => ({
        newUser: {
          ...prevState.newUser,
          age: value
        }
      }),
      () => console.log(this.state.newUser)
    );
  }

  handleInput(e) {
    let value = e.target.value;
    let name = e.target.name;
    this.setState(
      prevState => ({
        newUser: {
          ...prevState.newUser,
          [name]: value
        }
      }),
      () => console.log(this.state.newUser)
    );
  }


 

  handleFormSubmit(e) {
    e.preventDefault();
    let userData = this.state.newUser;

    fetch("http://example.com", {
      method: "POST",
      body: JSON.stringify(userData),
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    }).then(response => {
      response.json().then(data => {
        console.log("Successful" + data);
      });
    });
  }

  handleClearForm(e) {
    e.preventDefault();
    this.setState({
      newUser: {
        name: "",
        age: "",
        gender: "",
        skills: [],
        about: ""
      }
    });
  }

  componentDidMount(){
    if(window.location.pathname){
      this.currentState = window.location.pathname
    }
  };

  getTitle = ()=>{
    let str = window.location.pathname;
    let replaced = str.split('-').join(' ').toUpperCase();
    replaced = replaced.split('/');
    return replaced[1];
  }
 
  
  render() {
    if(window.location.pathname == "/"){
      return (
      <div className="col-md-6">
        <h3>Welcome To Dashboard</h3>
        <Button
          type={"primary"}
          title={"Add Player"}
          style={buttonStyle}
          link ={"/add-player"}
        />
        <Button
          type={"primary"}
          title={"Edit Player"}
          style={buttonStyle}
          link ={"/edit-player"}
        />
        <Button
          type={"primary"}
          title={"Search Form Player"}
          style={buttonStyle}
          link ={"/search-player"}
        />
      </div>
      ); 
    }else if (window.location.pathname == "/add-player"){
    return (
      <div className="col-md-6">
      <h3>{ this.getTitle() }</h3>
      <Button
          action={this.handleClick}
          type={"primary"}
          title={"Back Dashboard"}
          style={buttonStyle}
          link ={"/"}
        />
      <form className="container-fluid" onSubmit={this.handleFormSubmit}>
         {/* Username */}
        <Input
          inputType={"text"}
          title={"Username"}
          name={"name"}
          value={this.state.newUser.username}
          placeholder={"Enter your name"}
          handleChange={this.handleInput}
        />{" "}
        {/* Email */}
        <Input
          inputType={"text"}
          name={"email"}
          title={"Email"}
          value={this.state.newUser.email}
          placeholder={"Enter your email"}
          handleChange={this.handleInput}
        />{" "}
        {/* Password */}
        <Input
          inputType={"text"}
          name={"password"}
          title={"Password"}
          value={this.state.newUser.password}
          placeholder={"Enter your password"}
          handleChange={this.handleInput}
        />{" "}
         {/* Level */}
         <Input
          inputType={"number"}
          name={"level"}
          title={"Level"}
          value={this.state.newUser.level}
          placeholder={"Enter your level"}
          handleChange={this.handleInput}
        />{" "}
         {/* Exprience */}
         <Input
          inputType={"number"}
          name={"exprience"}
          title={"Exprience"}
          value={this.state.newUser.exprience}
          placeholder={"Enter your Exprienece"}
          handleChange={this.handleInput}
        />{" "}
        <Button
          action={this.handleFormSubmit}
          type={"primary"}
          title={"Submit"}
          style={buttonStyle}
        />{" "}
        {/* Clear the form */}
      </form>
      </div>
    );
    }else if (window.location.pathname == "/edit-player"){
      return (
        <div className="col-md-6">
        <h3>{ this.getTitle() }</h3>
        <Button
          action={this.handleClick}
          type={"primary"}
          title={"Back Dashboard"}
          style={buttonStyle}
          link ={"/"}
        />
        <form className="container-fluid" onSubmit={this.handleFormSubmit}>
           {/* Username */}
        <Input
          inputType={"text"}
          title={"Username"}
          name={"name"}
          value={this.state.newUser.username}
          placeholder={"Enter your name"}
          handleChange={this.handleInput}
        />{" "}
        {/* Email */}
        <Input
          inputType={"text"}
          name={"email"}
          title={"Email"}
          value={this.state.newUser.email}
          placeholder={"Enter your email"}
          handleChange={this.handleInput}
        />{" "}
        {/* Password */}
        <Input
          inputType={"text"}
          name={"password"}
          title={"Password"}
          value={this.state.newUser.password}
          placeholder={"Enter your password"}
          handleChange={this.handleInput}
        />{" "}
         {/* Level */}
         <Input
          inputType={"number"}
          name={"level"}
          title={"Level"}
          value={this.state.newUser.level}
          placeholder={"Enter your level"}
          handleChange={this.handleInput}
        />{" "}
        {/* Exprience */}
         <Input
          inputType={"number"}
          name={"exprience"}
          title={"Exprience"}
          value={this.state.newUser.exprience}
          placeholder={"Enter your Exprienece"}
          handleChange={this.handleInput}
        />{" "}
        <Button
          action={this.handleFormSubmit}
          type={"primary"}
          title={"Submit"}
          style={buttonStyle}
        />{" "}
        </form>
        </div>
      );
    }else if (window.location.pathname == "/search-player"){
      return (
        <div className="col-md-6">
        <h3>{ this.getTitle() }</h3>
        <Button
          action={this.handleClick}
          type={"primary"}
          title={"Back Dashboard"}
          style={buttonStyle}
          link ={"/"}
        />
        <form className="container-fluid" onSubmit={this.handleFormSubmit}>
          {/* Username */}
          <Input
            inputType={"text"}
            title={"Username"}
            name={"name"}
            value={this.state.newUser.username}
            placeholder={"Enter your name"}
            handleChange={this.handleInput}
          />{" "}
           {/* Email */}
          <Input
            inputType={"text"}
            name={"email"}
            title={"Email"}
            value={this.state.newUser.email}
            placeholder={"Enter your email"}
            handleChange={this.handleInput}
          />{" "}
          {/* Level */}
            <Input
            inputType={"number"}
            name={"level"}
            title={"Level"}
            value={this.state.newUser.level}
            placeholder={"Enter your level"}
            handleChange={this.handleInput}
          />{" "}
           {/* Exprience */}
          <Input
            inputType={"number"}
            name={"exprience"}
            title={"Exprience"}
            value={this.state.newUser.exprience}
            placeholder={"Enter your Exprienece"}
            handleChange={this.handleInput}
          />{" "}
          {/*Submit */}
          <Button
            action={this.handleFormSubmit}
            type={"primary"}
            title={"Submit"}
            style={buttonStyle}
          />{" "}
          
        </form>
        </div>
      );
    }
  }
}

const buttonStyle = {
  margin: "10px 10px 10px 10px"
};

export default FormContainer;
