import React from "react";

const Button = props => {

  const handleClick = (url) => {
    window.location = url
  } ;

  return (
    <button
      style={props.style}
      className={
        props.type == "primary" ? "btn btn-primary" : "btn btn-secondary"
      }
      onClick={()=>handleClick(props.link)}
    >
      {props.title}
    </button>
  );
};

export default Button;
