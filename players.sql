-- -------------------------------------------------------------
-- TablePlus 2.10(268)
--
-- https://tableplus.com/
--
-- Database: binarchapter8
-- Generation Time: 2021-03-21 09:02:23.0720
-- -------------------------------------------------------------


-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS players_id_seq;

-- Table Definition
CREATE TABLE "public"."players" (
    "id" int4 NOT NULL DEFAULT nextval('players_id_seq'::regclass),
    "username" varchar(255) NOT NULL,
    "email" varchar(255) NOT NULL,
    "password" varchar(255) NOT NULL,
    "experience" int4 NOT NULL,
    "lvl" int4 NOT NULL,
    "createdAt" timestamptz NOT NULL,
    "updatedAt" timestamptz NOT NULL,
    PRIMARY KEY ("id")
);

INSERT INTO "public"."players" ("id", "username", "email", "password", "experience", "lvl", "createdAt", "updatedAt") VALUES ('1', 'yoanes1', 'yoanesrn@gmail.com', '$2b$08$dL7Riy6ZRAo7Fiwu4bBzKeA98av.sKCYmyNn2sumf.KfLz6fPOR0O', '323', '2', '2021-03-20 09:51:47.866+07', '2021-03-20 14:21:33.079+07'),
('2', 'aaaaa', 'yoanesrn@gmail.com', '123456', '1', '1', '2021-03-20 09:51:54.992+07', '2021-03-20 14:21:11.839+07'),
('3', 'yoanes', 'yoanesrn@gmail.com', '123456', '3', '2', '2021-03-20 09:52:01.75+07', '2021-03-20 14:05:56.278+07'),
('4', 'yoanes', 'yoanesrn1@gmail.com', '1233456', '0', '0', '2021-03-20 10:21:45.614+07', '2021-03-20 10:21:45.614+07');
